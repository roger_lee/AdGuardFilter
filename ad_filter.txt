[Adblock Plus 2.0]
! Title: MyAdGuardFilter

forum.xda-developers.com###twig-top

gitee.com##div.gitee-stars-main-widget.pendan-widget

juejin.im###juejin div.request-health-alert

leangoo.com##div.el-dialog__wrapper
leangoo.com##div.v-modal

m.baidu.com###page-copyright > div:last-child

m.sohu.com##div.article-page div.feed-list-scoped > div

oschina.net###mainScreen > div.ui.container div.detail_banner_ad
oschina.net###mainScreen > div.ui.container div.float-adbox

v2ex.com###Rightbar > div.sep20:nth-child(3)
v2ex.com###Rightbar > div.box:nth-child(4)
v2ex.com###Rightbar > div.sep20:nth-child(5)